Feature: As a Internal trading engine consumer, I should be able to see fixtures

    Scenario: I should be able to see 3 fixtures when I make a GET request to the /fixtures endpoint
        Given I have the "fixtures" endpoint
        When I make a HTTP GET request
        Then I should see recieve a 200 HTTP response code
        And I should see 3 fixtures objects in the response
